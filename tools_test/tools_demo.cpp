/*! \file */

#include "pch.h"
#include <iostream>

#include "Logger.h"
#include "CPUInfo.h"

//! demo program showcasing tools
int main()
{

    using namespace skr::tools;

    std::cout << "Logger" << std::endl;
    Logger::GetInstance().Log("test test");

    std::cout << std::endl << "================================================================================" << std::endl << std::endl;
    std::cout << "CPUInfo" << std::endl;
    std::cout << CPUInfo::GetSystemInfo(true) << std::endl;
}

