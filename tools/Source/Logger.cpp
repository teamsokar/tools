#include "stdafx.h"
#include "Logger.h"

#include <ctime>

namespace skr
{
namespace tools
{
    Logger::Logger()
    {
        _logFilename = "Log_" + GetCurrentDateAndTime() + ".log";

        if (!_logFileStream.is_open())
            _logFileStream.open(_logFilename);
    }

    Logger::~Logger()
    {
        if(_logFileStream.is_open())
            _logFileStream.close();
    }

    void Logger::Log(std::string msg, LogLevel logLevel)
    {
        if (_logFileStream.is_open())
        {
            _logFileStream << msg << std::endl;
        }
    }

    void Logger::Log(std::vector<std::string> msgs, LogLevel logLevel)
    {
        for (auto& m : msgs)
        {
            Log(m);
        }
    }

    void Logger::LogWithDatenAndTime(std::string msg, LogLevel logLevel)
    {
        Log(GetCurrentDateAndTime() + " : " + msg, logLevel);
    }

    void Logger::LogNewLine()
    {
        if(_logFileStream.is_open())
            _logFileStream << std::endl;
    }

    void Logger::LogSeparationLine()
    {
        Log("--------------------------------------------------------------------------------");
    }

    std::string Logger::GetCurrentDateAndTime()
    {
        std::time_t t = std::time(nullptr);
        char mbstr[20];
#pragma warning (disable:4996)
        if(std::strftime(mbstr, sizeof(mbstr), "%Y%m%d_%H%M%S", std::localtime(&t)))
#pragma warning(default: 4996)
        {
            return std::string(mbstr);
        }
        else
        {
            std::cerr << "could not convet time to readable format" << std::endl;
        }

        return std::string();
    }
}
}
