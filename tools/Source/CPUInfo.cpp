#include "stdafx.h"
#include "CPUInfo.h"

#include <string>
#include <stdio.h>
#include <array>
#include <vector>
#include <VersionHelpers.h>
#include <intrin.h>

#ifdef _WIN32

std::string skr::tools::CPUInfo::ParseCPUArch(WORD arch)
{
    switch (arch)
    {
    case 0:  return std::string("x86");
    case 5:  return std::string("ARM");
    case 6:  return std::string("Itanium lol");
    case 9:  return std::string("AMD64");
    case 12: return std::string("ARM64");

    default: return std::string("unknown Processor Architectur");
    }
}

std::string skr::tools::CPUInfo::GetWindowsVersion()
{
    std::string wVer = std::string("older than Windows XP");

    if (IsWindowsXPOrGreater()) wVer = std::string("Windos XP");

    if (IsWindowsXPSP1OrGreater()) wVer = std::string("Windos XP SP1");
    if (IsWindowsXPSP2OrGreater()) wVer = std::string("Windos XP SP2");
    if (IsWindowsXPSP3OrGreater()) wVer = std::string("Windows XP SP3");

    if (IsWindowsVistaOrGreater()) wVer = std::string("Windows Vista");
    if (IsWindowsVistaSP1OrGreater()) wVer = std::string("Windows Vista SP1");
    if (IsWindowsVistaSP2OrGreater()) wVer = std::string("Windows Vista SP2");

    if (IsWindows7OrGreater()) wVer = std::string("Windows 7");
    if (IsWindows7SP1OrGreater()) wVer = std::string("Windows 7 SP1");

    if (IsWindows8OrGreater()) wVer = std::string("Windows 8");
    if (IsWindows8Point1OrGreater()) wVer = std::string("Windows 8.1");

    if (IsWindows10OrGreater()) wVer = std::string("Windows 10");

    wVer.append(IsWindowsServer() ? std::string(" Server") : std::string(" Client"));

    return wVer;
}

std::string skr::tools::CPUInfo::GetCPUVendor()
{
    std::array<int, 4> cpui;
    __cpuid(cpui.data(), 0);

    int nIds = cpui[0];
    std::vector<std::array<int, 4>> data;
    for (int i(0); i < nIds; ++i)
    {
        __cpuidex(cpui.data(), i, 0);
        data.push_back(cpui);
    }

    // Capture vendor string
    char vendor[0x20];
    memset(vendor, 0, sizeof(vendor));
    *reinterpret_cast<int*>(vendor) = data[0][1];
    *reinterpret_cast<int*>(vendor + 4) = data[0][3];
    *reinterpret_cast<int*>(vendor + 8) = data[0][2];

    return std::string(vendor);
}

std::string skr::tools::CPUInfo::GetCPUBrand()
{
    std::array<int, 4> cpui;
    __cpuid(cpui.data(), 0x80000000);

    int nExIds = cpui[0];
    char brand[0x40];
    memset(brand, 0, sizeof(brand));

    std::vector<std::array<int, 4>> extdata;
    for (int i(0x80000000); i <= nExIds; ++i)
    {
        __cpuidex(cpui.data(), i, 0);
        extdata.push_back(cpui);
    }

    if (nExIds >= 0x80000004)
    {
        memcpy(brand, extdata[2].data(), sizeof(cpui));
        memcpy(brand + 16, extdata[3].data(), sizeof(cpui));
        memcpy(brand + 32, extdata[4].data(), sizeof(cpui));
        return std::string(brand);
    }
    else
    {
        return std::string("No CPU brand string reported");
    }
}

std::string skr::tools::CPUInfo::GetSystemDetails(SYSTEM_INFO & siSysInfo)
{
    std::string sdstr;

    sdstr.append("\n================================================================================\n");
    sdstr.append("Hardware Information\n");

    sdstr.append("\tOEM ID: " + std::to_string(siSysInfo.dwOemId) + "\n");

    sdstr.append("\n\tCPU Details\n");
    sdstr.append("\tCPU Vendor: " + GetCPUVendor() + "\n");
    sdstr.append("\tCPU Brand: " + GetCPUBrand() + "\n");
    sdstr.append("\tProcessor Architecture: " + ParseCPUArch(siSysInfo.wProcessorArchitecture) + "\n");
    sdstr.append("\tNumber of Processors: " + std::to_string(siSysInfo.dwNumberOfProcessors) + "\n");

    sdstr.append("\n\tMemory Details\n");
    MEMORYSTATUSEX memStatEx;
    memStatEx.dwLength = sizeof(memStatEx);
    GlobalMemoryStatusEx(&memStatEx);
    sdstr.append("\tTotal Physical Memory " + std::to_string(memStatEx.ullTotalPhys / (DIV_GB)) + " GB\n");
    sdstr.append("\tTotal free Physical Memory " + std::to_string(memStatEx.ullAvailPhys / (DIV_GB)) + " GB\n");
    sdstr.append("\tUsed Memory " + std::to_string(memStatEx.dwMemoryLoad) + "%\n");

    sdstr.append("\tPage File\n");
    sdstr.append("\tTotal Page File Memory: " + std::to_string(memStatEx.ullTotalPageFile / (DIV_GB)) + " GB\n");
    sdstr.append("\tTotal Free Page File Memory: " + std::to_string(memStatEx.ullAvailPageFile / (DIV_GB)) + " GB\n");

    sdstr.append("\tVirtual Memory\n");
    sdstr.append("\tTotal Virtual Memory: " + std::to_string(memStatEx.ullTotalVirtual / (DIV_GB)) + " GB\n");
    sdstr.append("\tTotal Free Virtual Memory: " + std::to_string(memStatEx.ullAvailVirtual / (DIV_GB)) + " GB\n");
    sdstr.append("\tAvailable Extended Virtual Memory " + std::to_string(memStatEx.ullAvailExtendedVirtual / (DIV_GB)) + " GB\n");

    sdstr.append("\n\tWindows Settings\n");
    sdstr.append("\tWindows Version: " + GetWindowsVersion() + "\n");
    sdstr.append("\tPage size: " + std::to_string(siSysInfo.dwPageSize) + "\n");

    // type for those two is LPVOID, no idea how to get them to a std::string. Also not that interesting, so leave them out
    //sdstr.append("Minimum Application Address: " + std::to_string(siSysInfo.lpMinimumApplicationAddress) + "\n");
    //sdstr.append("Maximum Application Address: " + std::to_string(siSysInfo.lpMaximumApplicationAddress) + "\n");

    return sdstr;
}

std::string skr::tools::CPUInfo::GetSystemInfo(bool getNative)
{
    std::string sysInfo = "System Info\n";

    SYSTEM_INFO siSysInfo;
    GetSystemInfo(&siSysInfo);
    sysInfo = GetSystemDetails(siSysInfo);

    if (getNative)
    {
        sysInfo.append("\n================================================================================\nNative System Info\n");
        SYSTEM_INFO siNativeSysInfo;
        GetNativeSystemInfo(&siNativeSysInfo);
        sysInfo.append(GetSystemDetails(siNativeSysInfo));
    }

    sysInfo.append("\n================================================================================\n");

    return sysInfo;
}

#elif __linux__
// not implemented
#elif __APPLE__
// not implemented
#endif
