#pragma once

#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <filesystem>

namespace skr
{
namespace tools
{
    //! \class Logger
    //! \brief simple logger. Uses Scott Meyers singleton pattern
    class __declspec(dllexport) Logger
    {
        private:
            //! \brief default constructor
            //! opens filestream
            //! file stream is kept open for performance reasons
            Logger();

            //! \brief destructor
            //! closes file stream
            ~Logger();

            //! \brief copy constructor is deleted
            Logger(const Logger&) = delete;

            //! \brief copy assignment operator is deleted
            Logger operator=(const Logger&) = delete;

        public:
            //! \brief logging level
            //! specifies importance of message to be logged
            enum struct LogLevel
            {
                None = 0,       //!< no logigng at all
                Basic = 1,      //!< only very basic logging. Default value for most log functions
                Full = 3,       //!< full logging 
                Excessive = 4,  //!< log everything that doesn't defend itself
                Overwrite = 8   //!< log although the set logging level won't include the message
            };

        private:
            #pragma warning(disable:4251) // only using types from standard library, warning can be ignored. Source: https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4251
            std::string _logFilename; //!< filename of to log
            std::ofstream _logFileStream; //!< filestream
            #pragma warning(default:4251)

        public:
            //! \brief get instance of the logger
            //! implements Scott Meyers' singleton pattern
            static Logger& GetInstance()
            {
                static Logger instance;
                return instance;
            }

            //! \brief log specified message
            //! \param msg message to write to log
            //! \param logLevel importance level of message. Default is basic
            void Log(std::string msg, LogLevel logLevel = LogLevel::Basic);

            //! \brief log list of messages
            //! \param msgs list of messages to log
            //! \param logLevel importance level of message. Default is basic
            void Log(std::vector<std::string> msgs, LogLevel logLevel = LogLevel::Basic);

            //! \brief log message with current date and time
            //! \param msg message to write to log
            //! \param logLevel importance level of message. Default is basic
            void LogWithDatenAndTime(std::string msg, LogLevel logLevel = LogLevel::Basic);

            //! \brief insert new line into log
            void LogNewLine();

            //! \brief insert separation line into log
            void LogSeparationLine();

        private:
            //! \brief Gets current date and time in a readable and for filesystem suitable format
            std::string GetCurrentDateAndTime();
            
    };

}
}
#endif
