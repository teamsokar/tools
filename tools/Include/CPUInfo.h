#ifndef SKR_TOOLS_CPUINFO_H
#define SKR_TOOLS_CPUINFO_H

/*! \file */

#include <string>
#include <Windows.h>

#define DIV_KB 1024             //!< size of a kilobyte to divide by
#define DIV_MB 1024*1024        //!< size of a megabyte to divide by
#define DIV_GB 1024*1024*1024   //!< size of a gigabyte to divide by

namespace skr
{
namespace tools
{
namespace CPUInfo
{
#ifdef _WIN32
    //! \brief gets system information in humane readble form
    //! \param getNative also get information about native system, eg. if 32bit application is run on 64bit OS. Default is false
    std::string __declspec(dllexport) GetSystemInfo(bool getNative = false);

    //! \brief gets CPU architecture in human readable form
    //! \param arch CPU arch
    std::string ParseCPUArch(WORD arch);

    //! \brief gets CPU vender in human readable form
    std::string GetCPUVendor();

    //! \brief gets CPU brand in human readable form
    std::string GetCPUBrand();

    //! \brief gets version of Windows in human readable form
    std::string GetWindowsVersion();

    //! \brief gets system details in human readable form
    //! this includes CPU Vender, CPU brand, processor architetur, number of processor, windows version and details of memory used in run system
    //! \param siSysInfo system info to be parsed
    std::string GetSystemDetails(SYSTEM_INFO& siSysInfo);
#elif __linux__
    // not implemented
#elif __APPLE__
   // not implemented
#endif

}
}
}

#endif // ! SKR_TOOLS_CPUINFO_H
