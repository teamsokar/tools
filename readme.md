# Tools Library
Contains an assortment of tools written in C++

## Requirements
Build and tested with Microsoft Visual Studio 2019 (V 16.5.4) Community Edition and Windows SDK 10.0.17763.0.

## tools project
Contains the tools.  
Classes for export are declared with `__declspec(dllexport)`. For those classes, the warning [C4251](https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4251) is disable for members that use types from the standard library as they can be safely ignored. Warnings are only locally disabled.

### BasicGeometry.h
Static class containing definitions for basic geometries: cube, plane, quad and skybox. With vertex- and texture coordinates

### Logger.h
Simple logger class, writes messages to file in working directory of application. File name convention: "Log_YYYYMMDD_hhmmss.log". Uses [singleton](https://en.wikipedia.org/wiki/Singleton_pattern) pattern to enforce only one instance can exist at any time while making it available in any desired place.


## tools demo
A small demo program showcasing the usage and effects of the tool classes.
